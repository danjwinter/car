# Car
A simple car class with attributes `make`, `model`, and `color` along with actions to `start`, `stop`, and `get_info`.


[![miata](miata_in_the_mountains.jpeg)](https://www.linkedin.com/in/dan-winter-84398748/)

## Prerequisites

Install Ruby 2.5.1 using [rbenv](https://github.com/rbenv/rbenv#installation) or [rvm](https://rvm.io/rvm/install).

## Running Tests

``` bash
# Run all tests
$: ruby test/*_test.rb
```

## API

``` ruby
# Create a new car
# NOTE: make, model and color are required attributes.
first_car = Car.new({make: 'porsche', model: '911', color: 'black'}) 
# => #<Car:0x00007f8a43b765b8 @color='black', @engine_running=false, @make='porsche', @model='911'>

# The following default attributes are used if none are given:
# {color: 'silver', make: 'mazda', model: 'miata'}
car = Car.new 
# => #<Car:0x00007ffd35137ad8 @color='silver', @engine_running=false, @make='mazda', @model='miata'>

# Get color
car.color 
# => 'silver'

# Get make
car.make 
# => 'mazda'

# Get model
car.model 
# => 'miata'

# Get engine status, defaults to false
car.engine_running 
# => false

# Get info
car.get_info 
# => {:model=>'miata', :make=>'mazda', :color=>'silver', :engine_running=>false}

# Start engine
car.start 
# => #<Car:0x00007ffd35137ad8 @color='silver', @engine_running=true, @make='mazda', @model='miata'>

# Stop engine
car.stop 
# => #<Car:0x00007ffd35137ad8 @color='silver', @engine_running=false, @make='mazda', @model='miata'>

# Start and Stop are chainable methods
car.start.engine_running 
# => true
car.get_info 
# => {:model=>'miata', :make=>'mazda', :color=>'silver', :engine_running=>true}
car.stop.engine_running 
# => false
car.start.get_info 
# => {:model=>'miata', :make=>'mazda', :color=>'silver', :engine_running=>true}
```