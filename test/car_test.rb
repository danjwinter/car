require 'minitest/autorun'
require_relative '../lib/car'

class TestCar < Minitest::Test
  def setup
    @car = Car.new
  end

  def test_car_has_default_attributes
    assert_equal 'mazda', @car.make
    assert_equal 'miata', @car.model
    assert_equal 'silver', @car.color
    refute @car.engine_running
  end

  def test_car_can_get_info
    expected_info = {make: 'mazda', model: 'miata', color: 'silver', engine_running: false}
    assert_equal expected_info, @car.get_info
  end

  def test_car_can_start_and_stop_and_is_chainable
    started_expected_info = {make: 'mazda', model: 'miata', color: 'silver', engine_running: true}
    stopped_expected_info = {make: 'mazda', model: 'miata', color: 'silver', engine_running: false}

    refute @car.engine_running

    @car.start
    assert @car.engine_running
    assert_equal started_expected_info, @car.get_info

    @car.stop
    refute @car.engine_running
    assert_equal stopped_expected_info, @car.get_info

    assert_equal started_expected_info, @car.start.get_info
    assert_equal stopped_expected_info, @car.stop.get_info
  end

  def test_car_can_have_attributes_passed_at_instantiation
    other_car = Car.new({make: 'porsche', model: '911', color: 'black'})
    assert_equal 'porsche', other_car.make
    assert_equal '911', other_car.model
    assert_equal 'black', other_car.color
    refute other_car.engine_running

    expected_info = {make: 'porsche', model: '911', color: 'black', engine_running: false}
    assert_equal expected_info, other_car.get_info
  end

  def test_attributes_are_downcased
    yet_another_car = Car.new({make: 'Subaru', model: 'Impreza', color: 'black'})

    expected_info = {make: 'subaru', model: 'impreza', color: 'black', engine_running: false}
    assert_equal expected_info, yet_another_car.get_info
  end

  def test_it_raises_exception_when_passed_non_hash
    err = assert_raises(RuntimeError) { Car.new('porsche') }
    assert_match 'Attributes must be passed as a Hash during new Car instantiation.', err.message
  end

  def test_it_raises_exception_when_not_passed_make_model_and_color
    expected_error_message = 'Attributes must contain make, model, and color.'

    err = assert_raises(RuntimeError) { Car.new({make: 'porsche'}) }
    assert_match expected_error_message, err.message

    err = assert_raises(RuntimeError) { Car.new({model: '911'}) }
    assert_match expected_error_message, err.message

    err = assert_raises(RuntimeError) { Car.new({color: 'blue'}) }
    assert_match expected_error_message, err.message

    err = assert_raises(RuntimeError) { Car.new({}) }
    assert_match expected_error_message, err.message
  end
end
