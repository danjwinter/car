class Car
  attr_reader :color, 
              :engine_running,
              :make, 
              :model 

  def initialize(attributes = default_required_attributes)
    attributes = ensure_attributes(attributes)
    @color = attributes[:color]
    @make = attributes[:make]
    @model = attributes[:model]
    @engine_running = false
  end

  def get_info
    {
      color: color,
      engine_running: engine_running,
      make: make,
      model: model
    }
  end

  def start
    @engine_running = true
    self
  end

  def stop
    @engine_running = false
    self
  end

  private

  def default_required_attributes
    {
      make: 'mazda',
      model: 'miata',
      color: 'silver'
    }
  end

  def ensure_attributes(attributes)
    verify(attributes)
    standardized(attributes)
  end

  def verify(attributes)
    raise wrong_argument_type_message if attributes.class != default_required_attributes.class
    raise missing_attributes_message if missing_required?(attributes)
  end

  def standardized(attributes)
    Hash[attributes.each_pair.map{|k,v| [k.to_s.downcase.to_sym ,v.to_s.downcase]}]
  end

  def missing_required?(attributes)
    (default_required_attributes.keys - standardized(attributes).keys).any?
  end

  def wrong_argument_type_message
    "Attributes must be passed as a #{default_required_attributes.class} during new Car instantiation."
  end

  def missing_attributes_message
    "Attributes must contain #{format_as_sentence(default_required_attributes.keys)}."
  end

  def format_as_sentence(arr)
    case arr.length
    when 0
      ''
    when 1
      arr[0].to_s
    when 2
      "#{arr[0]} and #{arr[1]}"
    else
      "#{arr[0...-1].join(', ')}, and #{arr[-1]}"
    end
  end
end
